
class Ball
{
    public:
        int x, y;
        int startAlf;
        int maxX, maxY;
        bool vertical = false;
        bool sleep = false;
	int g;
	int y0;
	int x0;
	double dt;

	int r;
	int alf;
	int v0;
        int v0Init;

	double vx;
	double vy0;
	double t;


        Ball(int xSet, int ySet, int setDegree, int speed, bool vert);
        void prepareNextValue();
        void transformPosition();
        void position();
        void restart();


        void setScreen(int initX, int initY);
};