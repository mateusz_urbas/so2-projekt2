#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <ncurses.h>
#include <unistd.h>
#include <pthread.h> 
#include <thread>
#include <math.h>
#include <vector>
#include <ctime>

#include "Ball.h"
using namespace std;
#define DELAY 2000
bool status = true;

std::vector < Ball* > balls;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
int sleepingBalls = 0;

 int max_y = 0;
 int max_x = 0;

//---------------------------------------------------------------------

void moveBall(Ball * bal);
void addBalls();
void endProgram();

//---------------------------------------------------------------------

int main(int argc, char *argv[]) {

	initscr(); // daje globalne stdscr
	noecho();
	curs_set(FALSE);

	refresh();
	getmaxyx(stdscr, max_y, max_x);

	//Wątek Do tworzenia kulek
	thread generateBalls = thread(addBalls);
	thread pressButton = thread(endProgram);

	//Główny program, czyli wątek do wyświetlania
	while(status) {
		getmaxyx(stdscr, max_y, max_x);
		clear();
		
		mvprintw(max_y-1,(int)max_x/2-1, "|||||");
		
		//Wyświetlaj aktualne pozycje
		for( int i = 0; i < balls.size(); i++ ) {
			balls[i] -> setScreen(max_x,max_y);
			mvprintw(balls[i] -> y, balls[i] -> x, "o");
			//printw("   x=%d, y=%d t=%f\n", balls[i] -> x, balls[i] -> y, balls[i] ->t);
		}

		refresh();
		usleep(DELAY);
	}
 	endwin();

	generateBalls.join();
	pressButton.join();

	std::cout<<"Zakończono program...\n";
}



void moveBall(Ball * ball) 
{

	while(status){
		if(ball->v0 > 5 ) {
			ball -> transformPosition();
			usleep(DELAY);
			ball->prepareNextValue();

		} 
		else {
			pthread_mutex_lock(&mutex);
			sleepingBalls++; // liczenie uspionych bali, potrzebny mutex
			pthread_cond_wait(&cond, &mutex); 
			// usypianie wątku do czasu otrzymania sygnału
			ball -> restart();
			// ustawienie predkosci od nowa i czasu itp
			pthread_mutex_unlock(&mutex);
		}
	}


}
//----------------------------------------------------------------------

void addBalls() {
	srand( time( NULL ) );
  	vector<thread> threads;

	while(status) {

		//wysyłanie synału jezeli warunek
		if(sleepingBalls == 5) {
			sleepingBalls = 0;
			pthread_cond_broadcast(&cond);
		}

		if(balls.size() < 5) {
			int speed =( std::rand() % 35 ) + 10;
    		int option =( std::rand() % 3 ) + 1;
			bool vertical = false;

			//speed = 50;
			int degree;
			if(option==1) degree = 45;
			if(option==2) degree = 90;
			if(option==3){
				degree = 90;
				vertical = true;
			} 


			Ball* ball = new Ball((int)max_x/2,max_y-1,degree, speed, vertical);
			balls.push_back(ball);
			
			//Wątek do bali
			threads.push_back(thread(moveBall,ball));
			sleep(1);
		}
	}

	for( int i = 0; i < threads.size(); i++ ) {
		threads[i].join();
		threads[i].~thread();
	}

}


//---------------------------------------------------------------------

void endProgram() 
{
	unsigned char znak;

	while( znak != 113 ) { //znak q
		znak = getch();
	};

	for( int i = 0; i < balls.size(); i++ ) {
		delete balls[i];
	}
 
	status = false;
}
