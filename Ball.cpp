#include "Ball.h"
#include <math.h>
#include <unistd.h>

Ball::Ball(int xSet, int ySet, int setDegree, int speed, bool vert) {
	x = xSet; //aktualna pozycja x
	y = ySet; //aktualna pozycja y

	maxX = 0;	//max okno x
	maxY = 0; //max okno y

	g = 10; //przyspieszenie ziemskie
	y0 = ySet; //wys. początkowa
	x0 = xSet;
	dt = 0.002; //krok czasowy
	
	startAlf = setDegree;
	alf = setDegree; //kąt rzutu od poziomu

	v0=speed; //prędkość początkowa
	v0Init = speed;
	t = 0; //czas trwania rzutu
	vertical = vert;
	vx=v0*cos(alf); //wzorek
	vy0=v0*sin(alf); //wzorek

};

void Ball::restart() {
	v0 = v0Init;
}

void Ball::prepareNextValue() {

	double times = t + dt;
	int next_x = x0 + vx*times; // albo zmniejszasz albo zwiekszasz wiec idzie albo w prawo albo w lewo
	int next_y = y0 - vy0*times + g*times*times/2;

	//Odbijanie od boków
	if (next_x >= maxX|| next_x < 0) {
		y0 = next_y; //wys. początkowa
		x0 = next_x;

		v0-=v0/3; //nowa prędkość początkowa
		if(next_x >= maxX) alf = 180-alf;
		if(next_x < 0) alf = alf-180;

		vx=v0*cos(alf);
		vy0=v0*sin(alf);
		t = 0;
	}

	//Odbijanie od dołu i góry
	if (next_y >=  maxY || next_y < 0) {
		y0 = next_y; //wys. początkowa
		x0 = next_x;

		v0-=v0/3; //nowa prędkość początkowa

		if(next_y < 0 && next_x > maxX/2) alf = alf - 360;
		if(next_y < 0 && next_x < maxX/2) alf = alf + 360;
		
		if(next_y >=  maxY ) alf = startAlf;

		vx=v0*cos(alf);
		vy0=v0*sin(alf);
		t = 0;
	}
}

void Ball::transformPosition() {
	t+=dt; // zwiekszenie czasu o 1/100
	if(!vertical) x = x0 + vx*t; // nowa pozycja x
	y = y0 - vy0*t + g*t*t/2; //nowa pozycja y
}

void Ball::setScreen(int initX, int initY) {
	maxX = initX;
	maxY= initY;
}
